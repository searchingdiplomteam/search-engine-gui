import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { SearchModule } from './app/app.module';
import { LoginModule } from './app/app.module';
import { SettingsModule} from './app/app.module';

platformBrowserDynamic().bootstrapModule(SearchModule);
platformBrowserDynamic().bootstrapModule(LoginModule);
platformBrowserDynamic().bootstrapModule(SettingsModule);
