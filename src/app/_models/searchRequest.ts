/**
 * Created by Denis Biletsky on 11.04.2017.
 */
export class SearchRequest {
  request: string;
  domain: string;
}
