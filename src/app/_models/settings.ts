/**
 * Created by Denis Biletsky on 16.04.2017.
 */
export  class Settings {
  pages: number;
  threads: number;
  daysPeriod: number;
}
