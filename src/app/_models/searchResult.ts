/**
 * Created by Denis Biletsky on 11.04.2017.
 */
export class SearchResult {
  header: string;
  link: string;
  preview: string;
}
