import { Component } from '@angular/core';
import { SearchResult } from './_models/searchResult';
import { SearchRequest } from './_models/searchRequest';
import { SearchService } from './_service/searchService';

@Component({
  selector: 'search-component',
  template: `
  <div class="col s12">
    <div class="row">
        <form class="col s11 offset-s1">
            <div class="row">
                <div class="input-field col s11">
                    <i class="material-icons prefix">language</i>
                    <input id="domain" type="text" class="validate" placeholder="Domain">
                </div>
            </div>

            <div class="row">
                <div class="input-field col s11">
                    <i class="material-icons prefix">input</i>
                    <input id="request" type="text" class="validate" placeholder="Request">
                </div>
                <button (click)="getresults()" class="btn waves-effect waves-light "  ><i class="material-icons">search</i></button>
            </div>
        </form>
    </div>
  </div>
  <div *ngFor="let result of results" class="row">
    <div class="col s10 offset-s1">
      <div class="card teal lighten-5">
        <div class="card-content">
          <span class="card-title">{{result.header}}</span>
          <p>{{result.preview}}</p>
        </div>cd
        <div class="card-action">
          <a href="#">{{result.link}}</a>
        </div>
      </div>
    </div>
  </div>`,
  providers: [SearchService]
})
export class SearchComponent  {
  haveNoResults: boolean = true;
  results: SearchResult[] = [];
  request: SearchRequest;

  constructor(private searchService: SearchService) {}
  getresults() {
    this.results = this.searchService.results1;
    // this.searchService.getSearchResult(this.request)
    //  .subscribe((resp: Response) => this.results = resp.json());

    // this.haveNoResults = this.results.length === 0;
  }
}

@Component({
  selector: 'login-component',
  template: `
    <div class="row" style="padding: 30px">
      <form class="col s11 offset-s4" >
        <div class="row" >
          <div class="input-field col s5" >
            <input id="login" type="text" class="validate" placeholder="Login">
          </div>
        </div>
        <div class="row">
          <div class="input-field col s5">
            <input id="pass" type="text" class="validate" placeholder="Password" >
          </div>
        </div>
        <div class="col s3 offset-s1">
          <button class="btn waves-effect waves-light" (click)="login()" type="submit" name="action">LogIn
            <i class="material-icons right">input</i>
          </button>
        </div>
      </form>
    </div>`
})
export class LoginComponent {
  loginValue: string;
  passwordValue: string;
  login() {
    document.location.href = 'admin.html';
  }
}

@Component({
  selector: 'settings-component',
  template: `
    <div class="row" style="padding: 30px">
      <form class="col s11 offset-s1">
        <div class="row">
          <div class="input-field col s11">
            <i class="material-icons prefix">queue</i>
            <input id="domain" type="text" class="validate" placeholder="Count of page to crawl">
          </div>
        </div>
        <div class="row">
          <div class="input-field col s11">
            <i class="material-icons prefix">reorder</i>
            <input id="threads" type="text" class="validate" placeholder="Count of threads">
          </div>
        </div>
        <div class="row">
          <div class="input-field col s11">
            <i class="material-icons prefix">av_timer</i>
            <input id="days" type="text" class="validate" placeholder="Days to reload">
          </div>
        </div>
      </form>
      <div class="col s2 offset-s5">
        <button class="btn waves-effect waves-light" type="submit" name="action">Save
          <i class="material-icons right">done</i>
        </button>
      </div>
    </div>
  `
})
export  class  SettingComponrnt {
  sendSettings() {}
}
