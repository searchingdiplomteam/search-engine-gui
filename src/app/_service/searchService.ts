/**
 * Created by Biletskyi.D on 4/11/2017.
 */
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
// import {Response} from '@angular/http';
import {SearchRequest} from '../_models/searchRequest';
import {SearchResult} from '../_models/searchResult';

// import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class SearchService {
  // need server URL
  url: string = '';
  results1: SearchResult[] = [
    {preview: 'This is text that from page that contains search request 1', link: 'link', header: 'Link to page'},
    {preview: 'This is text that from page that contains search request 2', link: 'link', header: 'Link to page'},
    {preview: 'This is text that from page that contains search request 3', link: 'link', header: 'Link to page'}
  ];

  constructor(private http: Http) { }

  getSearchResult(obj: SearchRequest) {
    // Stub
    return this.results1;
    //  const body = JSON.stringify(obj);
    //  let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });

    // return this.http.post(this.url, body, { headers: headers })
    //   .map((resp: Response) => resp.json())
    //   .catch((error: any) => {return Observable.throw(error); });
  }
}
