import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule }   from '@angular/http';

import { SearchComponent }  from './app.component';
import { LoginComponent }  from './app.component';
import { SettingComponrnt }  from './app.component';

@NgModule({
  imports:      [ BrowserModule,
                  FormsModule,
                  HttpModule],
  declarations: [ SearchComponent ],
  bootstrap:    [ SearchComponent ]
})
export class SearchModule { }

@NgModule({
  imports:  [ BrowserModule,
              FormsModule,
              HttpModule],
  declarations: [LoginComponent],
  bootstrap: [LoginComponent]
})
export  class LoginModule { }

@NgModule({
  imports:  [ BrowserModule,
    FormsModule,
    HttpModule],
  declarations: [SettingComponrnt],
  bootstrap: [SettingComponrnt]
})
export  class SettingsModule { }
